﻿
using System;
using ChartWebview.iOS;
using Xamarin.Forms;
using Foundation;

[assembly: Dependency(typeof(BaseUrlNative))]

namespace ChartWebview.iOS
{
    class BaseUrlNative : IBaseUrlNative
    {
        public string Get()
        {
            return NSBundle.MainBundle.BundlePath;
        }
    }
}
