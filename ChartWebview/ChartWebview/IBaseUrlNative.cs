﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChartWebview
{
    public interface IBaseUrlNative
    {
        string Get();
    }
}
