﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

using System.IO;
using System.Reflection;

namespace ChartWebview
{
    public partial class MainPage : ContentPage
    {
        //public interface IBaseUrlNative { string Get(); }

        WebView webView;

        public MainPage()
        {

            InitializeComponent();

            webView = new WebView()
            {
                VerticalOptions = LayoutOptions.FillAndExpand,
                HorizontalOptions = LayoutOptions.FillAndExpand,
               // Source  = "http://uai.com.br"
            }; // temporarily use this so we can custom-render in iOS

			webView.Source = LoadHTMLFileFromResource();

			#region Solucao 1: em que o html, css e js dentro dos android_asset e funcionam dentro do WebView . Nota nao consegui chamar o javascript do pcl
			//instancia um htmlwebviewsource()
			//var htmlSource = new HtmlWebViewSource();

			//preenche ele com a pagina web dentro do asset android
			//htmlSource.Html = DependencyService.Get<IBaseUrlNative>().Get(); 
			// a pagina web esta preenchida desta 
			//
			#endregion

			#region Solucao 2: em que o html fica no localmente no PCL e so consome os js nos devices. Nota nao consigui chamar o javascript do pcl
			//instancia um webview 

			//instancia um htmlwebviewsource com codigo do html no pcl
			//var htmlSource = new HtmlWebViewSource();
			//htmlSource.Html = "<html><head><link rel=\"stylesheet\" href=\"default.css\"> " +
            //"<script src=\"teste.js\" type=\"text/javascript\" ></script>" +
            //"<script type=\"text/javascript\" src=\"nv.d3.js\"></script> " +
            //	"<script src=\"nv.de.min.js\" type=\"text/javascript\"></script>" +
            //	"</head><body><h1>Xamarin.Forms</h1><p>The CSS and image are loaded from local files!</p>" +
            //"<button onclick=\"meuAlerta();\">clickme </button> <p>thiago teste</p></body></html>";
            //passa o htmlSource como source do webview
            //webView.Source = htmlSource;
            //depois pega o javascript que esta dentro do asset e a pagina conforme o exemplo acima vai executar o javascript do device
            //htmlSource.BaseUrl = DependencyService.Get<IBaseUrlNative>().Get();

            #endregion

            #region Solucao 3: solucao aonde o html e os resources ficam no projeto PCL e os devices apenas utilizam os recursos do web view. 
            // a solucao foi baseada com este material https://developer.xamarin.com/recipes/cross-platform/xamarin-forms/controls/call-javascript/
            //o que eu fiz: coloquei o arquivo html na raiz do projeto e marquei em build action para a opcao EmbbeddeResource
            //Configuro a propriedadde do Source para receber o retorno do metodo

            //webView.Source = LoadHTMLFileFromResource();
            //O metodo LoadHTMLFileFromResource() retorna um HtmlWebViewSource que pega o arquivo html e renderiza com o metodo de stream io.
            //este metodo tambem e responsavel por pegar uma pagina que contem o xml da webview. - nao tenho certeza se este xml e necessario, ja que a classe esta vazia.
            // o metodo do click do botao faz uma chamada no javascript da pagina e passa ate parameetros. a troca de dados esta parcialmente ok.
            ///esta solucao precisa que sejam adicionados os arquivos que sao referenciados no html, tanto script quanto css, nas pastas de Asset do Android e Resources do iOS com a configuracao de build para AndroidAsset e BundleResource respectivamente.
            // Apos isso, as referencias do html tem que ser somente os arquivos js e css necessarios. Nao precisa informar o caminho dos arquivos nas tags link e script

            #endregion

            var btn = new Button()
            {
                Text = "botaozao"
            };

            btn.Clicked += OnCallJavaScriptButtonClicked;

            var stacklayout = new StackLayout();
            stacklayout.Children.Add(webView);
            stacklayout.Children.Add(btn);

            Content = stacklayout;
        }

        #region Metodos da solucao 3

        HtmlWebViewSource LoadHTMLFileFromResource()
        {
            var source = new HtmlWebViewSource();

            // Load the HTML file embedded as a resource in the PCL
            var assembly = typeof(WebViewPage).GetTypeInfo().Assembly;
            var stream = assembly.GetManifestResourceStream("ChartWebview.lineChartSVGResize.html");
            using (var reader = new StreamReader(stream))
            {
                source.Html = reader.ReadToEnd();
                source.BaseUrl = DependencyService.Get<IBaseUrlNative>().Get();
            }
            return source;
        }

        private void OnCallJavaScriptButtonClicked(object sender, EventArgs e)
        {
            webView.Eval(String.Format("alertaLocal({0})", 123));
        }

        #endregion
    }
}