﻿using ChartWebview.Droid;
using System;
using Xamarin.Forms;
using System.IO;

[assembly : Dependency(typeof (BaseUrlNativeAndroid))]
namespace ChartWebview.Droid
{
    public class BaseUrlNativeAndroid : IBaseUrlNative
    {
        public string Get()
        {
            return "file:///android_asset/";

            #region Solucao 1
            //working
            //var assetManager = Xamarin.Forms.Forms.Context.Assets;
            //using (var streamReader = new StreamReader(assetManager.Open("teste.html")))
            //{
            //	var html = streamReader.ReadToEnd();
            //             return html;

            //}
            #endregion

            #region Solucao 2 que ele carrega os elementos do asset
            //return "file:///android_asset/";

            #endregion
        }
    }
}